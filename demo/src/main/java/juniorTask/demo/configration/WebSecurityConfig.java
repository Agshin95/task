package juniorTask.demo.configration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().httpBasic().and().authorizeRequests().antMatchers("demo/public").permitAll()
                .antMatchers("/demo/user").hasAnyRole("USER", "ADMIN").
                antMatchers("/demo/admin").hasRole("ADMIN").anyRequest().authenticated();
       // super.configure(http);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("ram").
                password(passwordEncoder.encode("1234")).roles("ADMIN");
        auth.inMemoryAuthentication().withUser("ravan")
                .password(passwordEncoder.encode("12345")).roles("USER");
        auth.inMemoryAuthentication().withUser("kans")
                .password(passwordEncoder.encode("123456")).roles("USER");
    }

}
