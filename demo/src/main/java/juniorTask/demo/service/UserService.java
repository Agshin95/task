package juniorTask.demo.service;

import juniorTask.demo.UserDto;
import juniorTask.demo.model.UserEntity;

public interface UserService {
    void register(final UserDto user) ;
    boolean checkIfUserExist(final String email);
    void sendRegistrationConfirmationEmail(final UserEntity user);
    //boolean verifyUser(final String token) ;
    UserEntity getUserById(final String id) ;
}
