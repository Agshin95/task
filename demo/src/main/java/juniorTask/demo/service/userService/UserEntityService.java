package juniorTask.demo.service.userService;

import juniorTask.demo.UserDto;
import juniorTask.demo.model.UserEntity;
import juniorTask.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserEntityService {
    private final ModelMapper mapper;
    private final UserRepository userRepository;

   public UserDto  getUserId(Long id) {
       return userRepository.findById(id).map((s)->mapper.map(s,UserDto.class)).
               orElseThrow(RuntimeException::new);
   }

    public UserDto RegisterUser(UserDto dto) {
        UserEntity user = mapper.map(dto, UserEntity.class);
        UserEntity save =userRepository.save(user);
        return mapper.map(save, UserDto.class);
    }

    public void deletedUser(Long id) {
        userRepository.deleteById(id);
    }
    public UserDto upDateUSer(Long id, UserDto dto) {
        UserEntity user = userRepository.findById(id).orElseThrow(RuntimeException::new);
        dto.setId(user.getId());
        UserEntity save = userRepository.save(mapper.map(dto, UserEntity.class));
        return mapper.map(save, UserDto.class);
    }
}
