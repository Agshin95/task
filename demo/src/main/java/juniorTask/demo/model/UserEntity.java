package juniorTask.demo.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;


@Entity
@Data
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NAME_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(unique = true, name = "EMAIL")
    @Email
    private String email;

    @Column(name = "PASSWORD")
    private String password;

//    public UserEntity() {
//    }
}
