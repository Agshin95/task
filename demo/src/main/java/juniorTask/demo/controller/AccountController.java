package juniorTask.demo.controller;

import juniorTask.demo.UserDto;
import juniorTask.demo.service.userService.UserEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {
    private final UserEntityService userEntityService;

    @GetMapping("{id}")
    public UserDto getUser(@PathVariable Long id) {
        return userEntityService.getUserId(id);
    }
    public  UserDto createUser(@RequestBody UserDto dto){
        return userEntityService.RegisterUser(dto);
    }
    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id) {
        userEntityService.deletedUser(id);
    }
    @PutMapping("{id}")
    public UserDto userUpdate(@PathVariable Long id, @RequestBody UserDto dto){
        return userEntityService.upDateUSer(id, dto);
    }
}
