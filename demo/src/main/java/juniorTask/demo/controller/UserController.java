package juniorTask.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/demo")
public class UserController {

    @GetMapping("/user")
    public String getUser(){
        return "Hello User";
    }
    @GetMapping("/admin")
    public String getAdmin(){
        return "Hello Admin";
    }
    @GetMapping("/public")
    public String getPublic() {
        return "Hello Public";
    }
    }

